﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project1
{
    internal class OutputParametersDemo
    {
        static void Main()
        { int sum, subtract, product;
          float remainder;
            Operations(20, 10, out sum,
                out subtract,
                out product,
                out remainder);
            Console.WriteLine($"Sum is {sum}");
            Console.WriteLine($"Difference is {subtract}");
            Console.WriteLine($"Product is {product}");
            Console.WriteLine($"Remainder is {remainder}");
        

            }
        static void Operations(int x, int y, 
            out int add, out int difference,
            out int product,
            out float remainder)
        {
            add = x + y;
            difference = y - x;
            product = x * y;
            remainder = x % y;

        }
    }
}
