﻿namespace Project1
{
    internal class MethodOverloadingDemo
    {
        static int Add(int x, int y)
        {
            return x + y;
        }
        static int Add(int x, int y, int z)
        {
            return x + y + z;
        }
        static string Add(string s1, string s2)
        {
            return string.Concat(s1, " ", s2);
        }
        static float Add(int x, float y, float z)
        {
            return x + y + z;
        }
        static float Add(float x, int y, float z)
        {
            return x + y + z;
        }
        static void Main(string[] args)
        {
            Console.WriteLine(Add(10, 20));
            Console.WriteLine(Add("s1", "s2"));
            Console.WriteLine(Add(1,2,3));

        }
    }
}