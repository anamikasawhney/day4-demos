﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project1
{
    internal class ParamsArray
    {
        static void Main()
        {
            Console.WriteLine(Add(1,2));
            Console.WriteLine(Add(3,3,43,4,34,3,4,34,3,4,3,4,34,3,4,334,343,2));

        }
        static int Add(params int[] num)
        {
            int sum = 0;
            foreach (int i in num)
            {
                sum += i;
            }
            return sum;
        }
    }
}
