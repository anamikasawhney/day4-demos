﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project1
{
    internal class NamedParameter
    {
         static void Main()
        {
            DisplayDetails(id:10, name:"ajay", dept:"hr",
                salary:10998, exp:9);
            DisplayDetails(90000, "hr", "harpreet", 8, 9);
            DisplayDetails(salary: 98877, dept: "accts",
                name: "deepak", id: 100, exp: 8);

        }

        static void DisplayDetails(int id , string name,
            string dept, int salary, int exp)
        {
            Console.WriteLine($"Id > {id}\n" +
                $"Name > {name}\n" +
                $"Dept > {dept}\n" +
                $"Salary > {salary}\n" +
                $"Exp > {exp}");
        }
    }
}
